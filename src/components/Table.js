import React from 'react';
import '../css/Table.css';
import numeral from 'numeral';

const Table = ({countries}) => {

    return (
        <div className="table">
            {countries.map(country => (
                <tr key={country.countryInfo._id}>
                    <td>{country.country}</td>
                    <td><strong>{numeral(country.cases).format("0,0")}</strong></td>
                </tr>
                
            ))}
        </div>
    )
}

export default Table;