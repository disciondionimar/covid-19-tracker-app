import { Card, CardContent, Typography } from '@material-ui/core';
import React from 'react';
import '../css/InfoBox.css';

const InfoBox = ({title, cases, total, active, isRed, ...props}) => {
    return (
        <Card onClick={props.onClick} className={`infoBox ${active && 'infoBox--selected'} ${isRed &&'infoBox--red'}`}>
            <CardContent>
                {/* TItle, i.e. Coronavirus Cases */}
                <Typography className="infoBox__title" color="textSecondary">
                    {title}
                </Typography>

                {/* No. of Cases */}
                <h2 className={`infoBox__cases ${!isRed && 'infoBox__cases--green'}`}>{cases}</h2>

                {/* Total number of cases */}
                <Typography className="infoBox__total" color="textSecondary">
                    {total} Total
                </Typography>

            </CardContent>
            
        </Card>
    )
}

export default InfoBox
