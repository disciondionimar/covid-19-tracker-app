import React from 'react';
import '../css/Map.css';
import { MapContainer as LeafletMap, TileLayer } from "react-leaflet";
import ChangeView from './ChangeView';
import { showDataOnMap } from '../util';



// npm i leaflet

const Map = ({countries, casesType, center, zoom}) => {
console.log(casesType)

    return (
      <div className="map">
        <LeafletMap >
            <ChangeView  center={center} zoom={zoom} />
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {showDataOnMap(countries, casesType)}
        </LeafletMap>
      </div>
    );
}

export default Map
