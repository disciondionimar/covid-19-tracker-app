import { Circle, Popup } from 'react-leaflet';
import numeral from 'numeral';


const casesTypeColors = {
    cases: {
      hex: "#CC1034",
      multiplier: 300, //800
    },
    recovered: {
      hex: "#7dd71d",
      multiplier: 500, //1200
    },
    deaths: {
      hex: "#fb4443",
      multiplier: 800, //2000
    },
  };


export const sortData = data => {
    const sortedData = [...data];
    return sortedData.sort((a, b) => (a.cases > b.cases) ? -1 : 1);
}

export const prettyPrintStat = stat => 
    stat ? `+${numeral(stat).format("0.0a")}` : "+0";

export const firstLetterCapital = str => {
    const newStr = str.slice(0,1).toUpperCase() + str.slice(1)
    return newStr;
}

// Draw circles on the map with interactive tooltip
export const showDataOnMap = (data, casesType="cases") => {
    console.log(casesTypeColors[casesType].hex)
    return data.map(country => (
        
        <Circle
            pathOptions={{
                color:casesTypeColors[casesType].hex,
                fillColor:casesTypeColors[casesType].hex
            }}
            center={[country.countryInfo.lat, country.countryInfo.long]}
            fillOpacity={0.4}
            
            radius={Math.sqrt(country[casesType])*casesTypeColors[casesType].multiplier}
        >
            <Popup>
                <div className="info-container">
                    <div 
                        className="info-flag"
                        style={{backgroundImage: `url(${country.countryInfo.flag})`}}
                    />
                    <div className="info-name">{country.country}</div>
                    <div className="info-confirmed">Cases: {numeral(country.cases).format("0,0")}</div>
                    <div className="info-recovered">Recovered: {numeral(country.recovered).format("0,0")}</div>
                    <div className="info-deaths">Deaths: {numeral(country.deaths).format("0,0")}</div>
                    
                </div>
            </Popup>
        </Circle>
    ));
}

