import React, { useEffect, useState } from "react";
import "./App.css";
import { Card, CardContent, FormControl, MenuItem, Select } from "@material-ui/core";
import InfoBox from "./components/InfoBox";
import Map from "./components/Map";
import Table from "./components/Table";
import { firstLetterCapital, prettyPrintStat, sortData } from "./util";
import LineGraph from "./components/LineGraph";
import "leaflet/dist/leaflet.css";

function App() {
  const [countries, setCountries] = useState([]);
  const [country, setCountry] = useState("worldwide");
  const [countryInfo, setCountryInfo] = useState({})
  const [tableData, setTableData] = useState([]);
  const [mapCenter, setMapCenter] = useState([
    34.8076,
    -40.4796
  ]);
  const [mapZoom, setMapZoom] = useState(3);
  const [mapCountries, setMapCountries] = useState([]);
  const [casesType, setCasesType] = useState("cases");

console.log(casesType)
  

  const onCountryChange = async (e) => {
    const countryCode = e.target.value;

    const url = countryCode === 'worldwide' 
      ? "https://disease.sh/v3/covid-19/all" 
      : `https://disease.sh/v3/covid-19/countries/${countryCode}`

    await fetch(url)
      .then(res => res.json())
      .then(data => {
        // All of the data...
        // from the country response
        console.log(data)

        setCountry(countryCode);
        setCountryInfo(data);
        if(data.countryInfo){
          setMapCenter([data.countryInfo.lat, data.countryInfo.long])
          setMapZoom(5);
        } else {
          setMapCenter([34.8076,-40.4796])
          setMapZoom(3);
        }
        
        
        
      })

      
  };

  useEffect(() => {
    fetch("https://disease.sh/v3/covid-19/all")
      .then(res => res.json())
      .then(data => {
        setCountryInfo(data)
      })
  }, [])

  useEffect(() => {
    // this is where the code run once
    // when the component loads and not again.

    // Async code ==> send a request, wait for it and do something with the info.
    const getCountriesData = async () => {
      await fetch("https://disease.sh/v3/covid-19/countries")
        .then((res) => res.json())
        .then((data) => {
          const countries = data.map((country) => ({
            id: country.countryInfo._id,
            name: country.country,
            value: country.countryInfo.iso2,
          }));
          
          const sortedData = sortData(data);
          setTableData(sortedData);
          setMapCountries(data);
          setCountries(countries);
        });
    };

    getCountriesData();
  }, []);

  return (
    <div className="app">
      <div className="app__left">
        {/* Header */}
        <div className="app__header">
          <h1>COVID-19 Tracker</h1>
          <FormControl className="app__dropdown">
            <Select
              variant="outlined"
              value={country}
              onChange={onCountryChange}
            >
              {/* Loop through all the countries and show a drop down list of options */}
              <MenuItem value="worldwide">Worldwide</MenuItem>
              {countries.map((country) => (
                <MenuItem key={country.value+country._id} value={country.value}>
                  {country.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>

 
        {/* Info Boxes */}
        <div className="app__stats">
          <InfoBox 
            isRed
            active={casesType === "cases"}
            onClick={e => setCasesType('cases')}
            title="Coronavirus Cases" 
            cases={ prettyPrintStat(countryInfo.todayCases)} 
            total={ prettyPrintStat(countryInfo.cases)} 
          />
          <InfoBox 
            active={casesType === "recovered"}
            onClick={e => setCasesType('recovered')}
            title="Recovered" 
            cases={ prettyPrintStat(countryInfo.todayRecovered)} 
            total={ prettyPrintStat(countryInfo.recovered)} 
          />
          <InfoBox 
            isRed
            active={casesType==="deaths"}
            onClick={e => setCasesType('deaths')}
            title="Deaths" 
            cases={ prettyPrintStat(countryInfo.todayDeaths)} 
            total={ prettyPrintStat(countryInfo.deaths)} 
          />
        </div>  

        {/* Map */}
        <Map 
          casesType={casesType}
          countries={mapCountries}
          center={mapCenter}
          zoom={mapZoom}
        />
      </div>
      <Card className="app__right">
        <CardContent>
            {/* Table  */}
            <h3>Live Cases by Country</h3>
            <Table countries={tableData}/>
            {/* Graph */}
            <h3 className="app__graphTitle">Worldwide New {firstLetterCapital(casesType)}</h3>
            <LineGraph country={country} className="app__graph" casesType={casesType}/>
        </CardContent>
                
      </Card>
    </div>
  );
}

export default App;
